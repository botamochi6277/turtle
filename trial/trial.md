<!-- author: Naoki Kubota  -->
# 体験会用資料 v1.1

## まずさわってみよう！
### Arduinoって?
[Arduino](https://ja.wikipedia.org/wiki/Arduino#cite_note-1)(アルドゥイーノ)はオープンソースのマイコンです．マイコン，つまりマイクロコントローラは，多くの電気製品で頭脳のような役割を果たしています．たとえば，冷蔵庫の中の温度を一定に保ったり，電子レンジで「温め」を使ったときに，ご飯がちょうどいい温度に温められるのは，マイコンが機械を「制御」しているからです．

Arduinoは，だれもが機械の制御を簡単にできるように開発されたマイコンで，非常に扱いやすくなっています．最近ではエンジニアだけではなく，アーティストなども利用しており世界的に人気のあるモジュールです．

<!-- ![arduino_and_genuino](https://www.arduino.cc/en/uploads/Main/A000066_iso_both.jpg) -->
<img src="https://www.arduino.cc/en/uploads/Main/A000066_iso_both.jpg" height=200px>


--*https://www.arduino.cc/en/Main/ArduinoBoardUno*
### Arduinoに話してもらいましょう．
まずはArduinoのソフトを起動しましょう．
デスクトップに下の図のようなアイコンがあるので，ダブルクリックしてください．

![](img/arduino_icon.png)

起動したら，左上の「ファイル」をクリックして，「スケッチの例」→「04. Communication」→　「SerialEvent」の順にポインタ（画面上の矢印）を動かし，最後に「SerialEvent」をクリックしましょう．
できたら，SerialEventのプログラムが画面に表示されます．

![open_eg_sketch](img/open_eg_sketch.png)

次にこのプログラムを少し改造してみましょう．
55行目したの図のように
```
inputString += ""
```
から
```
inputString += "said Hello!"
```
に書き換えましょう．

![change_serial](img/change.png)

続いて，ファイルを保存しましょう．左上の「ファイル」をクリックして，「保存」をクリックしましょう．

![](img/save_sketch0.png)

ファイルの名前を聞かれるので，ローマ字で自分の名前を打って，右下の「Save」ボタンをクリックして保存しましょう．
![save_sketch](img/save_sketch1.png)

次にArduinoにこのプログラムを書き込みます．
左上の「スケッチ」をクリックして，次に「マイコンボードに書き込む」をクリックしましょう．

![compile](img/compile.png)

Arduinoへの書き込みに成功すると左下に「ボードへの書き込みが完了しました」と表示されます．

![](img/writing.png)

Arduinoに新しいプログラムが書き込まれると，自動的にArduinoは新しいプログラムを実行します．

プログラムが実行されているか確認しましょう．
「ツール」→「シリアルモニタ」の順にクリックすると，シリアルモニタというArduinoとの通信ウィンドウが開きます．

![](img/serial_monitor.png)

シリアルモニタの上のコンソールに適当な文字を入れてEnterを押しましょう（送信）．すると，下の図のように「打った文字」＋「said 'Hello'」が表示されます．

![echo](img/serial_echo.png)

今回書き込んだプログラムはecho（やまびこ）プログラムと呼ばれていて，Arduinoに送った文字をArduinoがやまびこのように返事をしてくれるプログラムです．

## LEDを光らせよう！
### ブレッドボードって？
ブレッドボードは電子回路を簡単に試作するための基板(足場)です．ブレッドボードにモータや電池などをを挿すことで電子回路が作れます．

<!-- ![breadboard](https://upload.wikimedia.org/wikipedia/commons/7/73/400_points_breadboard.jpg) -->
<img src="https://upload.wikimedia.org/wikipedia/commons/7/73/400_points_breadboard.jpg" height=200px>

--*wikipedia*

ブレッドボードは下の図でいうと，赤色で塗りつぶされている部分が繋がっています．そのため，2つの線を跨いでいる右の緑のLEDは点灯しますが，左の赤のLEDには電流が流れないので，点灯しません．

![orientation](./img/board-orientation.jpg)

<!-- [^1]:大人の事情で日本ではGenuino(ジェヌイーノ)という名前に変わりました．でも，中身は完全に同じです．
[^2]:ルールを守れば，誰でも自由に使ってもいい技術やデザインのこと -->

### LEDって？
LEDは非常に効率のいいライトです．LEDには赤・緑・青のものがあり，電流を流すことで光ります．注意して欲しいのが，LEDには向きがあることです．足の長い方が+側，足の短い方が-側(GND)側になります．

<!-- ![](http://k-itxserver.ddo.jp/~nrs/special/arduino_03/arduino03_led02.jpg) -->
<img src="http://k-itxserver.ddo.jp/~nrs/special/arduino_03/arduino03_led02.jpg" height=200px>

--*k-itxserver.ddo.jp/*


### 抵抗って？
下の部品は電気抵抗，あるいは抵抗と呼ばれています．回路に流れる電流をコントロールするために使われています．川の水門の電流版だと思ってください．
<!-- ![](http://akizukidenshi.com/img/goods/C/R-25102.JPG) -->
<img src="http://akizukidenshi.com/img/goods/C/R-25102.jpg" height=200px>

--*http://akizukidenshi.com/catalog/g/gR-25102/*

### 回路を作ろう
まずはLEDを光らせる下の図の回路を以下の手順で作りましょう．
1. 1 kΩの抵抗の足を直角をコの字になるように曲げてブレッドに挿す．
2. LEDの短い方の足を抵抗の右側の足と同じ列になるように挿す．
3. 抵抗の左側の列に青いコードを挿す．
4. 青いコードの反対側をArduinoの「GND」と書かれた穴（ソケット）に挿す．
5. 赤いコードをLEDの右側の列に挿す．
6. 赤いコードの反対側をArduinoの「~3」と書かれた穴（ソケット）に挿す．

これで回路は完成です．

![led_board](img/Ledbrink_breadboard.png)

### 光らせよう
スケッチの例から「Blink」を開きましょう．

![open_blink](img/open_blink.png)

最初LEDを制御するピン番号が13となっているので，これを回路に合わせて3に変更していきます．
`pinMode(13,OUTPUT);`を`pinMode(3,OUTPUT);`に，`digitalWrite(13,OUTPUT);`を`digitalWrite(3,OUTPUT);`にそれぞれ変更しましょう．
![](img/blink_change.png)

先ほどと同じように「スケッチ」→「マイコンボードに書き込む」の順にクリックすると，このプログラムがArduinoに書き込まれます．
このプログラムを実行すると下の動画のようにLEDが点滅します．

![blink](img/blink.gif)

### 点滅の速さを変えよう
26, 28行目の`delay`の値を変更しましょう．
`delay(x)`は x msec(1/1000秒)，arduinoが処理を止めて待つ関数です．たとえば，`delay(1000)`は1000msec, 1秒待つ処理となります．

![](img/change_delay.png)

`delay(100;)`にすると，
下の動画のようにLEDが早く点滅します（5Hz）.

![](img/blink_fast.gif)

## センサの値を読み取ろう
### ポテンショメータ
下の写真のパーツはポテンショメータといって角度を測るときに使うパーツです．音量の調整やロボットの関節角の計測に使われています．

<img src="http://akizukidenshi.com/img/goods/C/P-06110.jpg" height=200px>
<!-- ![](http://akizukidenshi.com/img/goods/C/P-06110.jpg) -->

--*http://akizukidenshi.com/catalog/g/gP-06110/*
### 回路を作ろう
このポテンショメータを使って，角度を測っていきます．はじめに下の図のような回路を作りましょう．

1. ポテンショメータをブレッドボードに挿す（向きに注意！）．
2. ポテンショメータの右側の列とArduinoの「GND」のソケットを青いコードでつなぐ．
3. ポテンショメータの真ん中の列とArduinoの「A0」のソケットを黄色のコードでつなぐ．
2. ポテンショメータの左側の列とArduinoの「5V」のソケットを赤いコードでつなぐ．

![](img/potentiometer_breadboard.png)
### プログラムを作ろう
「スケッチの例」から「AnalogReadSerial」のファイルを開きましょう．

![](img/open_analogread.png)

次に22行目を
```
Serial.println(sensorValue);
```
から
```
Serial.println((float)sensorValue/1024*340.0);
```
![](img/analog_change.png)

と変更してください．このプログラムをArduinoに書き込んで実行すると，シリアルモニタに現在のセンサの角度が表示されます．ポテンショメータを回して，値が変わることを確認しましょう．

![](img/analog_monitor.png)

### ポテンショメータでLEDを制御しよう

LEDの点滅間隔をポテンショメータで制御してみましょう．
まずは，下の図の回路を組みましょう．

![](img/potentiometer_led_breadboard.png)

次に下のプログラムをArduinoに書き込みましょう．ポテンショメータのプログラムからの変更箇所は青線の部分です．LEDの点滅プログラムからコピーしてきましょう．
![](img/led_potentiomater.png)

このプログラムを実行すると，下の動画のようにポテンショメータでLEDの点滅間隔を制御することができます．

![](img/blink_change.gif)

---
&copy; Turtle Education, 2016

#include "pitches.h"

#define echoPin 12 // Echo Pin
#define trigPin 11 // Trigger Pin

#define speakerPin 8 // Spearker Pin

double Duration = 0; //受信した間隔
double Distance = 0; //距離
void setup() {
  Serial.begin( 9600 );
  pinMode( echoPin, INPUT );
  pinMode( trigPin, OUTPUT );

}
void loop() {
  // ドレミファソラシド
  unsigned int melody[] = {
  NOTE_C4, NOTE_D4, NOTE_E4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_B4, NOTE_C5
  };
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite( trigPin, HIGH ); //超音波を出力
  delayMicroseconds( 10 ); //
  digitalWrite( trigPin, LOW );
  Duration = pulseIn( echoPin, HIGH ); //センサからの入力
  if (Duration > 0) {
    Duration = Duration / 2; //往復距離を半分にする
    Distance = Duration * 340 * 100 / 1000000; // 音速を340m/sに設定
    Serial.print("Distance:");
    Serial.print(Distance);
    Serial.println(" cm");
    if (Distance < 23.0) { // 音がなる距離を設定
//      unsigned int freq = map(Distance * 10, 0, 255, 0, 4096);
      unsigned int freq = melody[(unsigned int)(Distance/3.0)];
      tone(speakerPin, freq);
    } else {
      noTone(speakerPin);
    }
  } else {
    Serial.println("Nothing!");
  }
  delay(100);
}
